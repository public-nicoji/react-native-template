import {Locale} from './locales';

const jsonLang: {[lang: string]: Record<string, string>} = {
  en: require('./en.json'),
  fr: require('./fr.json'),
};

export const translations = {
  [Locale.ENGLISH]: jsonLang.en,
  [Locale.FRENCH]: jsonLang.fr,
};
