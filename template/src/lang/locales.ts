export enum Locale {
  ENGLISH = 'en-US',
  FRENCH = 'fr-FR',
}
