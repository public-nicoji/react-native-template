import {combineReducers, configureStore} from '@reduxjs/toolkit';
import {userReducer} from './users';
import {UserState} from './users/type';

export type RootState = {
  user: UserState;
};

const rootReducer = combineReducers<RootState>({
  user: userReducer,
});

const store = configureStore({
  reducer: rootReducer,
});

export default store;
