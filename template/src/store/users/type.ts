import {Locale} from '../../lang/locales';

export interface UserState {
  id: number | null;
  email: string | null;
  pushPermission: boolean;
  theme: ThemeType | null;
  locale: Locale;
  isLogged: boolean;
}

export enum ThemeType {
  DARK = 'dark',
  LIGHT = 'light',
  SYSTEM = 'system',
}
