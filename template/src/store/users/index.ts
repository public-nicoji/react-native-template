import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {ThemeType, UserState} from './type';
import {Locale} from '../../lang/locales';

const initialState: UserState = {
  id: null,
  email: null,
  pushPermission: false,
  theme: null,
  locale: Locale.ENGLISH,
  isLogged: false,
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setLogin: (state, action: PayloadAction<boolean>) => {
      state.isLogged = action.payload;
    },
    setPushPermission: (state, action: PayloadAction<boolean>) => {
      state.pushPermission = action.payload;
    },
    setTheme: (state, action: PayloadAction<ThemeType | null>) => {
      state.theme = action.payload;
    },
    setLocale: (state, action: PayloadAction<Locale>) => {
      console.log(action.payload);
      state.locale = action.payload;
    },
  },
});

export const {setLogin, setPushPermission, setTheme, setLocale} =
  userSlice.actions;
export const userReducer = userSlice.reducer;
