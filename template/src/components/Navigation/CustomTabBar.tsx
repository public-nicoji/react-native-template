import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {BottomTabBarProps} from '@react-navigation/bottom-tabs';
import Icon, {IconType} from '../Icons';

const BACKGROUND = 'red';

function CustomTabBar({state, navigation}: BottomTabBarProps) {
  const UNFOCUSED = '#222';
  const FOCUSED = '#673ab7';

  return (
    <View style={styles.container}>
      {state.routes.map((route, index) => {
        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name, route.params);
          }
        };

        return (
          <TouchableOpacity
            key={route.key}
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={'Tap bar menu | ' + route.name}
            onPress={onPress}
            style={{
              paddingVertical: 15,
              paddingHorizontal: 10,
            }}>
            <View style={{alignItems: 'center'}}>
              <Icon type={IconType.HomeActive} width={20} height={20} />
            </View>
            <Text style={{color: isFocused ? FOCUSED : UNFOCUSED}}>
              {route.name}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

export default CustomTabBar;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: BACKGROUND,
    justifyContent: 'space-around',
    paddingBottom: 15,
  },
});
