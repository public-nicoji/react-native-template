import React, {FunctionComponent} from 'react';
import {StyleSheet, Text, TextProps} from 'react-native';
import {FormattedMessage} from 'react-intl';

export type AppTextType =
  | 'TitleLarge'
  | 'TitleMedium'
  | 'TitleSmall'
  | 'BodyLarge'
  | 'BodyMedium'
  | 'BodySmall';

interface Props extends TextProps {
  type: AppTextType;
  message: string;
}

const AppText: FunctionComponent<Props> = ({
  type,
  style,
  message, // Can be replace by children from TextProps if no use of react-intl - COMMENT
  ...props
}) => {
  return (
    <Text style={[textStyles[type], style]} {...props}>
      <FormattedMessage id={message} />
    </Text>
  );
};

export default AppText;

export const textStyles = StyleSheet.create({
  TitleLarge: {
    fontFamily: 'Inter-SemiBold',
    fontSize: 32,
    lineHeight: 37,
  },
  TitleMedium: {
    fontFamily: 'Inter-SemiBold',
    fontSize: 24,
    lineHeight: 29,
  },
  TitleSmall: {
    fontFamily: 'Inter-SemiBold',
    fontSize: 18,
    lineHeight: 26,
  },
  BodyLarge: {
    fontFamily: 'Inter-Regular',
    fontSize: 20,
    lineHeight: 24,
  },
  BodyMedium: {
    fontFamily: 'Inter-Regular',
    fontSize: 16,
    lineHeight: 19,
  },
  BodySmall: {
    fontFamily: 'Inter-Regular',
    fontSize: 14,
    lineHeight: 17,
  },
});
