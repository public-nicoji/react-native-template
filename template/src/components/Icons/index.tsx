import React, {FunctionComponent} from 'react';
import {ImageProps} from 'react-native';
import {SvgProps} from 'react-native-svg';

import HomeIcon from './assets/navigation/home-icon.svg';
import ChevronLeftIcon from './assets/navigation/chevron-left.svg';

export enum IconType {
  HomeInactive,
  HomeActive,
  ChevronLeft,
}

interface IconTypeProps extends Omit<ImageProps, 'source'> {
  type: IconType;
  color?: SvgProps['color'];
}

const Icon: FunctionComponent<IconTypeProps> = ({type, ...props}) => {
  const assets = {
    [IconType.HomeActive]: <HomeIcon color="blue" {...props} />,
    [IconType.HomeInactive]: <HomeIcon color="black" {...props} />,
    [IconType.ChevronLeft]: <ChevronLeftIcon color="black" {...props} />,
  };

  return assets[type];
};

export default Icon;
