import {FunctionComponent, useEffect, useRef} from 'react';
import {Animated, StyleSheet, View} from 'react-native';
import AppText from '../AppText';

export enum ToastType {
  DEFAULT = 'default',
  SUCCESS = 'success',
  WARNING = 'warning',
  DANGER = 'danger',
}

interface ToastProps {
  type: ToastType;
  message: string;
}

export const toastColors = {
  [ToastType.DEFAULT]: {
    backgroundColor: '#333',
    borderColor: '#d3d3d3',
    textColor: 'white',
    defaultText: 'Notification',
  },
  [ToastType.SUCCESS]: {
    backgroundColor: 'rgb(46, 125, 50)',
    borderColor: '#d3d3d3',
    textColor: 'white',
    defaultText: 'Success',
  },
  [ToastType.WARNING]: {
    backgroundColor: 'rgb(237, 108, 2)',
    borderColor: '#d3d3d3',
    textColor: 'white',
    defaultText: 'Warning',
  },
  [ToastType.DANGER]: {
    backgroundColor: 'rgb(211, 47, 47)',
    borderColor: '#d3d3d3',
    textColor: 'white',
    defaultText: 'Carefull',
  },
};

const Toast: FunctionComponent<ToastProps> = ({type, message}) => {
  const {backgroundColor, textColor, borderColor, defaultText} =
    toastColors[type];

  const slideAnimation = useRef(new Animated.Value(-100)).current;

  useEffect(() => {
    Animated.timing(slideAnimation, {
      toValue: 80,
      duration: 500,
      useNativeDriver: true,
    }).start();

    setTimeout(() => {
      Animated.timing(slideAnimation, {
        toValue: -100,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }, 4500);
  }, []);

  return (
    <Animated.View
      style={[styles.container, {transform: [{translateY: slideAnimation}]}]}>
      <View style={[styles.alert, {backgroundColor, borderColor}]}>
        <AppText
          type="BodyMedium"
          style={{textAlign: 'center', color: textColor}}
          message={defaultText}
          id={message}
        />
      </View>
    </Animated.View>
  );
};

export default Toast;

export const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    width: '100%',
    justifyContent: 'center',
    alignSelf: 'center',
    zIndex: 1,
  },
  alert: {
    width: '75%',
    borderRadius: 10,
    borderWidth: 2,
    height: 60,
    justifyContent: 'center',
    alignSelf: 'center',
  },
});
