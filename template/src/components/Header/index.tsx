import React, {FunctionComponent, ReactNode} from 'react';
import {View, StyleSheet, StyleProp, TextStyle} from 'react-native';
import {
  SafeAreaView,
  SafeAreaViewProps,
  useSafeAreaInsets,
} from 'react-native-safe-area-context';
import useTheme from '../../utils/useTheme';

import AppText from '../AppText';
import Colors from '../../utils/Colors';
interface Props extends SafeAreaViewProps {
  title?: string | (() => ReactNode);
  leftSection?: () => ReactNode;
  rightSection?: () => ReactNode;
  titleStyle?: StyleProp<TextStyle>;
}

const Header: FunctionComponent<Props> = ({
  title,
  leftSection,
  rightSection,
  style,
  titleStyle,
}: Props) => {
  const theme = useTheme();
  const topInset = useSafeAreaInsets().top;

  const renderTitle = () => {
    if (typeof title === 'string') {
      return (
        <AppText
          type="TitleLarge"
          numberOfLines={1}
          style={[styles.title, titleStyle]}
          ellipsizeMode={'tail'}
          message={title}
        />
      );
    }
    return title?.();
  };

  return (
    <SafeAreaView
      edges={['top']}
      style={[
        styles.container,
        theme.backgroundColor,
        {
          height: 56 + topInset,
          borderBottomColor: Colors.BLACK_100,
        },
        style,
      ]}>
      <View style={styles.areaLeft}>{leftSection?.()}</View>
      {renderTitle()}
      <View style={styles.areaRight}>{rightSection?.()}</View>
    </SafeAreaView>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    paddingHorizontal: 16,
  },
  title: {
    maxWidth: '50%',
  },
  areaLeft: {
    flexGrow: 1,
    flexBasis: 0,
  },
  areaRight: {
    flexGrow: 1,
    flexBasis: 0,
  },
});
