import React, {FunctionComponent} from 'react';
import {
  TouchableOpacity,
  View,
  useWindowDimensions,
  StyleSheet,
  TouchableOpacityProps,
} from 'react-native';
import AppText from '../AppText';
import Colors from '../../utils/Colors';
import useTheme from '../../utils/useTheme';
import Icon, {IconType} from '../Icons';
import {ThemeType} from '../../store/users/type';

interface Props extends TouchableOpacityProps {
  title?: string;
  hideText?: boolean;
}

const HeaderBack: FunctionComponent<Props> = ({title, hideText, ...props}) => {
  const theme = useTheme();
  const isSmallScreen = useWindowDimensions().width < 350;
  const isDarkMode = theme.type === ThemeType.DARK;

  const renderIcon = () => {
    return <Icon type={IconType.ChevronLeft} />;
  };

  return (
    <TouchableOpacity {...props}>
      <View style={styles.container}>
        {renderIcon()}
        {!isSmallScreen && !hideText && (
          <AppText
            type="BodyMedium"
            style={[styles.text, isDarkMode && theme.text]}
            message={title ?? 'Back'}
          />
        )}
      </View>
    </TouchableOpacity>
  );
};

export default HeaderBack;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    fontSize: 16,
    lineHeight: 20,
    color: Colors.BLACK_100,
  },
});
