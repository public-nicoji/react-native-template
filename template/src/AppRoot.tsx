import React from 'react';
// import {NativeModules} from 'react-native'; TODELETE if no use of locale device
import {useSelector} from 'react-redux';
import {IntlProvider} from 'react-intl';
import {RootState} from './store';
import AppNavigator from './components/Navigation/index';
// import {Locale} from './lang/locales'; TODELETE if no use of locale device
import {translations} from './lang/translations';

/* Get locale data from the device if needed, or can be deleted - TODELETE

// iOS:
const localeIOS =
  NativeModules.SettingsManager.settings.AppleLocale ||
  NativeModules.SettingsManager.settings.AppleLanguages[0];

// Android:
const localeAndroid = NativeModules.I18nManager.localeIdentifier;

const locale: Locale =
  localeIOS.replace('_', '-') ??
  localeAndroid.replace('_', '-') ??
  Locale.ENGLISH;
*/

const AppRoot = () => {
  const userLocale = useSelector((state: RootState) => state.user.locale);

  return (
    <IntlProvider locale={userLocale} messages={translations[userLocale]}>
      <AppNavigator />
    </IntlProvider>
  );
};

export default AppRoot;
