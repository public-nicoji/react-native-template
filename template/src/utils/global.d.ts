declare module '*.svg' {
  import React from 'react';
  import {SvgProps} from 'react-native-svg';

  interface Props extends SvgProps {
    fill1?: string;
    fill2?: string;
  }

  const content: React.FC<Props>;
  export default content;
}
