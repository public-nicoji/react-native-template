import {StatusBarStyle, useColorScheme} from 'react-native';
import {useSelector} from 'react-redux';
import {ThemeType} from '../store/users/type';
import Colors from './Colors';
import {RootState} from '../store';

const useTheme = (forcedTheme?: ThemeType) => {
  const userTheme = useSelector((state: RootState) => state.user.theme);
  const systemTheme = useColorScheme();

  const themes: any = {
    [ThemeType.LIGHT]: {
      type: ThemeType.LIGHT,
      barStyle: 'dark-content' as StatusBarStyle,
      background: {backgroundColor: Colors.WHITE_100},
    },
    [ThemeType.DARK]: {
      type: ThemeType.DARK,
      barStyle: 'light-content' as StatusBarStyle,
      background: {backgroundColor: Colors.BLACK_100},
    },
  };

  if (userTheme === ThemeType.SYSTEM) {
    return themes[forcedTheme ?? systemTheme ?? ThemeType.LIGHT];
  }

  const theme =
    themes[forcedTheme ?? userTheme ?? systemTheme ?? ThemeType.LIGHT];

  return theme;
};

export default useTheme;
