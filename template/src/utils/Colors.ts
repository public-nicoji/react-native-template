/**
 * File to store all the global colors of the app
 *
 * Color palette link :
 */
enum Colors {
  BLACK_100 = '#000000',
  WHITE_100 = '#FFFFFF',
}

export default Colors;
