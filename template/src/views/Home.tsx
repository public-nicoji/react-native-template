import {FunctionComponent} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import AppText from '../components/AppText';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {setLocale} from '../store/users';
import {Locale} from '../lang/locales';
import {useDispatch} from 'react-redux';
import Header from '../components/Header/index';
import HeaderBack from '../components/Header/HeaderBack';
import Toast, {ToastType} from '../components/Toast';

const Home: FunctionComponent = () => {
  const dispatch = useDispatch(); // TEST

  return (
    <>
      {/* TEST */}
      <Header leftSection={() => <HeaderBack />} title="Test" />
      <Toast type={ToastType.DEFAULT} message="attention" />
      <View style={styles.container}>
        <AppText type="TitleLarge" message="home.title" />
        <TouchableOpacity onPress={() => dispatch(setLocale(Locale.FRENCH))}>
          <Text>Change</Text>
          {/* TEST */}
        </TouchableOpacity>
      </View>
    </>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
