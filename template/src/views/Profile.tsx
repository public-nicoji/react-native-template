import {FunctionComponent} from 'react';
import {View, StyleSheet, Text} from 'react-native';

const Profile: FunctionComponent = () => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'center',
      }}>
      <Text>Profile</Text>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({});
