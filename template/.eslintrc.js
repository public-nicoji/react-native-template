module.exports = {
  root: true,
  env: {
    es6: true,
    node: true,
    jest: true,
  },
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react-native/all',
  ],
  rules: {
    complexity: ['error', 10],
    'no-unused-vars': 'error',
    camelcase: 'error',
    '@typescript-eslint/no-inferrable-types': 'error',
  },
};
