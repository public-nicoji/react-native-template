import React from 'react';
import {Provider} from 'react-redux';
import store from './src/store';
import AppRoot from './src/AppRoot';

const App = () => {
  return (
    <Provider store={store}>
      <AppRoot />
    </Provider>
  );
};

export default App;
